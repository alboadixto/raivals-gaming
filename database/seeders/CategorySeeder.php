<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name'=> 'Notebooks',
                'slug' => Str::slug('Notebooks'),
                'icon' => '<i class="fas fa-laptop"></i>'
            ],
            [
                'name'=> 'Hardware y Accesorios',
                'slug' => Str::slug('Hardware y Accesorios'),
                'icon' => '<i class="fas fa-headset"></i>'
            ],
        ];

        foreach($categories as $category){
            $category = Category::factory(1)->create($category)->first();

            $brands = Brand::factory(4)->create();

            foreach($brands as $brand){
                $brand->categories()->attach($category->id);
            }
        }
    }
}
