<?php

namespace Database\Seeders;

use App\Models\Subcategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SubcategorySeeder extends Seeder
{

    public function run()
    {
        $subcategories = [
            /* Notebook */
            [
                'category_id' => 1,
                'name'=> 'Notebooks Gamers',
                'slug' => Str::slug('Notebooks Gamers')
            ],

            /* Hardware y Accesorios */
            [
                'category_id' => 2,
                'name'=> 'Celulares y smartphones',
                'slug' => Str::slug('Celulares y smartphones')
            ],
            [
                'category_id' => 2,
                'name'=> 'Routers',
                'slug' => Str::slug('Routers')
            ],
        ];

        foreach($subcategories as $subcategory){
            Subcategory::factory(1)->create($subcategory);
        }
    }
}
